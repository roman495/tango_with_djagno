import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango.settings')

import django
django.setup()

from rango.models import Category, Page


def populate():
    language_pages = [
        {'title': 'Python',
         'url': 'https://en.wikipedia.org/wiki/Python_(programming_language)',},
        {'title': 'Ruby',
         'url': 'https://en.wikipedia.org/wiki/Ruby_(programming_language)',},
        {'title': 'PHP',
         'url': 'https://en.wikipedia.org/wiki/PHP',},
    ]

    framework_pages = [
        {'title': 'Django',
         'url': 'https://en.wikipedia.org/wiki/Django_(web_framework)',},
        {'title': 'Ruby on Rails',
         'url': 'https://en.wikipedia.org/wiki/Ruby_on_Rails',},
        {'title': 'Symphony',
         'url': 'https://en.wikipedia.org/wiki/Symfony',},
    ]

    frontend_pages = [
        {'title': 'HTML',
         'url': 'https://en.wikipedia.org/wiki/HTML',},
        {'title': 'CSS',
         'url': 'https://en.wikipedia.org/wiki/Cascading_Style_Sheets',},
        {'title': 'JavaScript',
         'url': 'https://en.wikipedia.org/wiki/JavaScript',},
    ]

    cats = {
        'Language': {'pages': language_pages},    
        'Framework': {'pages': framework_pages},    
        'Frontend': {'pages': frontend_pages},
    }

    for cat, cat_data in cats.items():
        c = add_cat(cat)
        for p in cat_data['pages']:
            add_page(c, p['title'], p['url'])

    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print('- {0} - {1}'.format(str(c), str(p)))

def add_page(cat, title, url, views=0, likes=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url = url
    p.views = views
    p.likes = likes
    p.save()
    return p

def add_cat(name):
    c = Category.objects.get_or_create(name=name)[0]
    c.save()
    return c


if __name__ == '__main__':
    print("Starting Rango population script...")
    populate()
