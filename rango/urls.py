from django.conf.urls import url, include

from .views import (
	IndexView,
	AboutView,
	CategoryList,
	CategoryDetail,
	CategoryCreate,
	CategoryUpdate,
	CategoryDelete,
	PageCreate,
	PageUpdate,
	PageDelete,
)

app_name = 'rango'

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^categories/$', CategoryList.as_view(), name='categories'),
    url(r'^category/(?P<slug>[\w\-]+)/$', CategoryDetail.as_view(), name='category'),
    url(r'^categories/add-category/$', CategoryCreate.as_view(), name='add_category'),
    url(r'^category/(?P<slug>[\w\-]+)/update/$', CategoryUpdate.as_view(), name='edit_category'),
	url(r'^category/(?P<slug>[\w\-]+)/delete/$', CategoryDelete.as_view(), name='delete_category'),
    url(r'^category/(?P<slug>[\w\-]+)/add-page/$', PageCreate.as_view(), name='add_page'),
    url(r'^page/(?P<slug>[\w\-]+)/update/$', PageUpdate.as_view(), name='edit_page'),
	url(r'^page/(?P<slug>[\w\-]+)/delete', PageDelete.as_view(), name='delete_page'),
]
