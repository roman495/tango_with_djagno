from django.contrib import admin

from .models import Category, Page


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'views')
    prepopulated_fields = {'slug': ('name',)}
    readonly_fields = ('views',)


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'category', 'url', 'likes')
    list_display_links = ('title', 'slug')
    list_filter = ('category__name',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ['title']
    readonly_fields = ('likes',)

    fieldsets = (
        ('Basic', {'fields': ('category', 'title', 'url', 'slug')}),
    )
