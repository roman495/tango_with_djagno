from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView


from .models import Category, Page
from .forms import CategoryForm, PageForm


class IndexView(TemplateView):
    template_name = "rango/index.html"


class AboutView(TemplateView):
    template_name = "rango/about.html"


class CategoryList(ListView):
    model = Category
    context_object_name = 'categories'


class CategoryDetail(DetailView):
    model = Category
    context_object_name = 'category'

    def get_object(self):
        category = super(CategoryDetail, self).get_object()
        category.views += 1
        category.save()
        return category


class CategoryCreate(LoginRequiredMixin, CreateView):
    model = Category
    form_class = CategoryForm
    template_name_suffix = '_create'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(CategoryCreate, self).form_valid(form)


class CategoryUpdate(LoginRequiredMixin, UpdateView):
    model = Category
    form_class = CategoryForm
    template_name_suffix = '_update'

    def get_object(self):
        category = super(CategoryUpdate, self).get_object()
        if category.owner != self.request.user:
            raise Http404("Access is denied")
        return category


class CategoryDelete(LoginRequiredMixin, DeleteView):
    model = Category
    context_object_name = 'category'
    template_name_suffix = '_delete'
    success_url = reverse_lazy('rango:categories')

    def get_object(self):
        category = super(CategoryDelete, self).get_object()
        if category.owner != self.request.user:
            raise Http404("Access is denied")
        return category


class PageCreate(LoginRequiredMixin, CreateView):
    model = Page
    context_object_name = 'page'
    form_class = PageForm
    template_name_suffix = '_create'

    def get_context_data(self, **kwargs):
        context = super(PageCreate, self).get_context_data(**kwargs)
        context['category'] = get_object_or_404(Category, slug=self.kwargs['slug'])
        return context

    def form_valid(self, form):
        self.category = get_object_or_404(Category, slug=self.kwargs['slug'])
        form.instance.owner = self.request.user
        form.instance.category = self.category
        form.instance.views = 0
        return super(PageCreate, self).form_valid(form)


class PageUpdate(LoginRequiredMixin, UpdateView):
    model = Page
    context_object_name = 'page'
    form_class = PageForm
    template_name_suffix = '_update'

    def get_object(self):
        page = super(PageUpdate, self).get_object()
        if page.owner != self.request.user:
            raise Http404("Access is denied")
        return page


class PageDelete(LoginRequiredMixin, DeleteView):
    model = Page
    context_object_name = 'page'
    template_name_suffix = '_delete'

    def get_object(self):
        page = super(PageDelete, self).get_object()
        if page.owner != self.request.user:
            raise Http404("Access is denied")
        return page

    def get_success_url(self):
        return reverse('rango:category', kwargs={'slug': self.object.category.slug})
