from django.conf.urls import url
from django.contrib.auth.views import login

from .views import LogoutView, RegisterView

app_name = 'accounts'

urlpatterns = [
    url(r'^login/$', login, {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register')
]
