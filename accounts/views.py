from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.urls import reverse_lazy


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('rango:index')


class RegisterView(FormView):
    template_name = 'accounts/register.html'
    form_class = UserCreationForm
    success_url = reverse_lazy('rango:index')

    def form_valid(self, form):
        new_user = form.save()
        username = new_user.username
        password = form.cleaned_data['password1']
        authenticated_user = authenticate(username=username, password=password)
        login(self.request, authenticated_user)
        return super(RegisterView, self).form_valid(form)
